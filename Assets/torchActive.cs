﻿using UnityEngine;
using System.Collections;

public class torchActive : MonoBehaviour {

	public GameObject GM;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.gameObject.tag == "Player")
		{
			Debug.Log("Entered");
			GM.SetActive(true);
		}
	}

	void OnTriggerExit(Collider col)
	{
		if(col.gameObject.tag == "Player")
		{

			Debug.Log("Exit");
			GM.SetActive(false);
		}
	}
}
