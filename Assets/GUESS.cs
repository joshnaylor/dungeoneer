﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUESS : MonoBehaviour {


	public AudioClip wrongSound;
	public AudioClip won;
	public static Text text;
	static string code = "crony";
	static bool winner = false;
	static bool wrong = false;
	// Use this for initialization
	void Start () {
		text = GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if(winner)
		{
			winning();
			winner = false;
		}

		if(wrong)
		{
			wrongAn();
			wrong = false;
		}
	}

	public static void Append(string ch)
	{
		int length = 0;
		if (text.text == "*****") 
		{
			text.text = ch;
		}
		text.text = text.text + ch;
		Debug.Log (text.text);
		for(int i = 0; i < text.text.Length; i++)
		{
			length++;
		}
		if(length == 5)
		{
			if(text.text == code)
			{
				//text.text = ch;
				Debug.Log("WINNNNNER");
				winner = true;

			}
			else
			{
				Debug.Log("Wrong, Clear");
				text.text = "";
				wrong = true;
			}
		}
	}

	void winning()
	{
		Debug.Log("WINNNNNER, Play audio");
		audio.PlayOneShot (won);
		//go to exit scene
		Invoke ("End", 24);

	}

	void wrongAn()
	{
		Debug.Log("WRONG, Play audio");
		audio.PlayOneShot (wrongSound);
	}

	void End()
	{
		Application.LoadLevel (3);
	}

}
