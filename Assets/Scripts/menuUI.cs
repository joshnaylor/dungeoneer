﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class menuUI : MonoBehaviour {

	public int goToLevel;
	public Text textArea;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("Scroll", 5, 0.05f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Scroll ()
	{
		Vector2 rectPos = textArea.rectTransform.anchoredPosition;
		Debug.Log(rectPos);
		if(transform.position.y <= 5)
		{
			transform.position += new Vector3(0,0.05f,0);
		}
		else if(rectPos.y >= 90)
		{
			Application.LoadLevel(goToLevel);
		}
		else 
		{
			textArea.transform.position += new Vector3(0,0.05f,0);
		}
	}
}
