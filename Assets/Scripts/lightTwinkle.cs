﻿using UnityEngine;
using System.Collections;

public class lightTwinkle : MonoBehaviour {

	public float low = 0.01f;
	public float high = 1.0f;
	Light torch;
	float intensity; 
	// Use this for initialization
	void Start () {
		torch = GetComponent<Light> ();
		InvokeRepeating ("Fire", 0f, 0.15f);
	}
	
	// Update is called once per frame
	void Fire () {
		intensity = Random.Range(low,high);
		//torch.intensity = intensity;
		torch.intensity = Mathf.Lerp (torch.intensity, intensity, 0.2f);
		// light.intensity = Mathf.Lerp(light.intensity, newIntensity, smooth * Time.deltaTime);
	}
}
