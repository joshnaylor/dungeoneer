﻿using UnityEngine;
using System.Collections;

public class RoomSwitcher : MonoBehaviour {

	public Transform roomFrom;
	public Transform roomTo;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider other)
	{
		audio.Play();
		if(other.gameObject.tag == "Player" && gameObject.tag == "RoomTo")
		{
			Debug.Log(roomTo.position);
			other.gameObject.transform.position = new Vector3(roomTo.position.x,roomTo.position.y, roomTo.position.z - 1);
		}

		if(other.gameObject.tag == "Player" && gameObject.tag == "RoomFrom")
		{
			Debug.Log(roomTo.position);
			other.gameObject.transform.position = new Vector3(roomTo.position.x,roomTo.position.y, roomTo.position.z + 1);
			other.gameObject.transform.rotation = roomTo.rotation;
		}
	}
}
