﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (CharacterController))]
[System.Serializable]
public class Player : MonoBehaviour {

	CharacterController controller;
	bool moving;
	GameObject head;

	public void Start () {
		
		controller = this.gameObject.GetComponent ("CharacterController") as CharacterController;
		this.gameObject.AddComponent ("CharacterMotor");
		head = GameObject.Find ("ALPSHead");
		if (Application.platform == RuntimePlatform.Android) {
			moving = false;
		}
	}

	public void Update () {
		
		if (Input.GetAxis ("Vertical") > 0.1f) 
		{
			if (Application.platform == RuntimePlatform.Android)
			{
				if (!moving)
				{
					ALPSAndroid.Vibrate(20);
					moving = true;
				}
			}
			controller.Move (new Vector3 (head.transform.forward.x, 0, head.transform.forward.z) * Time.deltaTime * 3);
		}
		else if (Input.GetAxis ("Vertical") < -0.1f) 
		{
			if (Application.platform == RuntimePlatform.Android)
			{
				if (!moving)
				{
					ALPSAndroid.Vibrate(20);
					moving = true;
				}
			}
			controller.Move (new Vector3 (-head.transform.forward.x, 0, -head.transform.forward.z) * Time.deltaTime * 3);
		}
		else 
		{
			if (Application.platform == RuntimePlatform.Android){
				if(moving)moving=false;
			}
		}
	}
}
